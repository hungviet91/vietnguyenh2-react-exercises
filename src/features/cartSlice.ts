import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Product } from '../components/Products/types';
import { RootState } from '../core/store';

type CartState = {
  cartItems: Array<Product>;
};

const initialState: CartState = {
  cartItems: [],
};

export const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    addToCart: (state, action: PayloadAction<Product>) => {
      // Don't add duplicated item
      if (state.cartItems.some(item => item.id === action.payload.id)) return;
      state.cartItems.push(action.payload);
    },
    removeFromCart: (state, action: PayloadAction<number>) => {
      state.cartItems = state.cartItems.filter(
        item => item.id !== action.payload
      );
    },
  },
});

// Use select to prevent touching the whole store.
export const selectCartItems = (state: RootState) => state.cart.cartItems;

export const cartActions = cartSlice.actions;
const cartReducer = cartSlice.reducer;
export default cartReducer;
