import { Product } from '../../components/Products/types';
import { useAppDispatch } from '../../core/store';
import { cartActions } from '../cartSlice';

const CartItem = ({ item }: { item: Product }) => {
  const dispatch = useAppDispatch();
  return (
    <div className="flex justify-between gap-2">
      <span>{item.title}</span>
      <button onClick={() => dispatch(cartActions.removeFromCart(item.id))}>
        <svg
          width={24}
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth="1.5"
          stroke="currentColor"
          className="w-6 h-6"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M6 18L18 6M6 6l12 12"
          />
        </svg>
      </button>
    </div>
  );
};

export default CartItem;
