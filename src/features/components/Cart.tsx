import { useEffect, useRef, useState } from 'react';
import { useAppSelector } from '../../core/store';
import useClickOutside from '../../hooks/useClickOutside';
import { selectCartItems } from '../cartSlice';
import styles from './Cart.module.css';
import CartItem from './CartItem';
const Cart = () => {
  const ref = useRef(null);
  const [displayCart, setDisplayCart] = useState(false);
  const cartItems = useAppSelector(selectCartItems);

  useClickOutside(ref, () => setDisplayCart(false));

  useEffect(() => {
    if (cartItems.length === 0) setDisplayCart(false);
  }, [cartItems]);

  return (
    <div className={styles.wrapper} ref={ref}>
      <button className="btn" onClick={() => setDisplayCart(state => !state)}>
        View cart
      </button>
      {displayCart && (
        <div className={styles.cartList}>
          {cartItems.length === 0
            ? 'There is no items'
            : cartItems.map(item => <CartItem key={item.id} item={item} />)}
        </div>
      )}
    </div>
  );
};

export default Cart;
