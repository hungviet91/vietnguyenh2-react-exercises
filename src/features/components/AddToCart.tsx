import { Product } from '../../components/Products/types';
import { useAppDispatch, useAppSelector } from '../../core/store';
import { cartActions, selectCartItems } from '../cartSlice';

type Props = {
  product: Product;
};

const AddToCartBtn = ({ product }: Props) => {
  const distpach = useAppDispatch();
  const cartItems = useAppSelector(selectCartItems);
  const isAddedToCart = cartItems.some(p => p.id === product.id);

  return (
    <>
      {isAddedToCart ? (
        <button
          className="btn"
          onClick={() => distpach(cartActions.removeFromCart(product.id))}
        >
          Remove from cart
        </button>
      ) : (
        <button
          className="btn"
          onClick={() => distpach(cartActions.addToCart(product))}
        >
          Add to cart
        </button>
      )}
    </>
  );
};

export default AddToCartBtn;
