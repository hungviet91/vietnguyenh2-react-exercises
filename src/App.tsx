import { Provider } from 'react-redux';
import './App.css';
import Header from './components/Header';
import Products from './components/Products';
import { store } from './core/store';

function App() {
  return (
    <Provider store={store}>
      <Header />
      <Products />
    </Provider>
  );
}

export default App;
