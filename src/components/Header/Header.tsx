import Cart from '../../features/components/Cart';
import styles from './Header.module.css';
const Header = () => {
  return (
    <div className={styles.wrapper}>
      <h1 className={styles.headint}>React exercises</h1>
      <Cart />
    </div>
  );
};

export default Header;
