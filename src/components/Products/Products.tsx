import { useEffect, useState } from 'react';
import ProductItem from './ProductItem';
import styles from './Products.module.css';
import { Product } from './types';
const Products = () => {
  const [products, setProducts] = useState<Product[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState('');

  useEffect(() => {
    (async function () {
      try {
        setIsLoading(true);
        const response = await fetch('https://fakestoreapi.com/products');
        if (!response.ok) {
          throw 'Cannot fetch data';
        }
        const data = await response.json();
        setProducts(data);
      } catch (error) {
        if (error instanceof Error) {
          setError(error.message);
        } else [setError('Unexpected error')];
      } finally {
        setIsLoading(false);
      }
    })();
  }, []);

  if (isLoading) return <>Loading</>;
  if (error) return <>error</>;

  if (products.length === 0) return <>There is no product</>;

  return (
    <div className={styles.wrapper}>
      {products.map(product => (
        <ProductItem key={product.id} {...product} />
      ))}
    </div>
  );
};

export default Products;
