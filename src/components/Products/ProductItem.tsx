import AddToCartBtn from '../../features/components/AddToCart';
import styles from './ProductItem.module.css';
import { Product } from './types';
const ProductItem = (props: Product) => {
  const { title, description, image, price } = props;
  return (
    <div className={styles.wrapper}>
      <img width={300} height={300} src={image} alt={title} loading="lazy" />
      <h3>{title}</h3>
      <strong>{price}$</strong>
      <p className={styles.description}>{description}</p>
      <AddToCartBtn product={props} />
    </div>
  );
};

export default ProductItem;
